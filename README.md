# The Challenge
Given the attached schema, feel free to edit the schema as you see fit for the purposes of this exercise.
The schema represents EPG (Electronic Program Guide) data for channels.

Your task is write an importer that reads EPG data from the provided XML file and writes it to the database models.
Only existing channel’s schedules should be imported.

There are several providers for EPG data and each provider uses a different format.
The formats mostly used are XML, CSV, JSON, Web Services etc.

Where possible, your solution should allow the support of multiple providers with an ability to switch between any one of them.
Configurability, code re-use and readability are what we look for.

# Note

* There could be missing fields in the provider’s data. Not all providers have complete EPG fields.
Some providers have only basic EPG data while others have enriched EPG data. If a field is not found in the XML, please skip it.

* Attached file descriptions
    
    * *epg_schema.sql* contains the schema for the EPG tables. Also has data for the channels we’re interested in.

    * *xml_descr.png* shows nodes of interest in the xml file
 
    * *egp.xml.zip* is the provider’s xml for EPG data
    
# The solution 
* I changed the schema a bit to match the given xml file. Some of the changes are
	* service_livetv_program table is a chiled table of service_livetv_schedule; so I added a foreign key shchedule_id to service_livetv_program table
	* schedule_id should be unique but i found repeated schedule_id in the xml, so i generated unique external schedule_id by concatinating channel_id and schedule_id. This also changes the column's datatype in the schema 
	* also generated unique program_id in a similar way.
	* I changed some of the column datatypes and default values from the given schema. Ex. end_time( to allow null), iso_3_lang( column name and character size)
* Configure the initial values using Config class, the application can run with the default config values. 
* I changed some of the given xml data to json to test json importer functionality. 
* To run the application call save_epg function from EPG class.    
 
