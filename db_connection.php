<?php
require_once __DIR__ . '/config.php';

class DB_Connection {
    private $_link = null;

    public function open_connection() {
        $this->_link = mysqli_init();
        $config = new Config();
        $this->_link->real_connect( $config->getDbHost(),
                                    $config->getDbUserName(),
                                    $config->getDbPassword(),
                                    $config->getDbDatabase() );

        if( mysqli_connect_error() ) {
            die('Connect Error ('.mysqli_connect_errno().') '.mysqli_connect_error());
        }
    }

    public function close_connection() {
        if( $this->_link ) {
            $this->_link->close();
        }
    }

    public function run_query( $query ) {
        $result = mysqli_query($this->_link, $query);

        if( !$result ) {
            die('Error: ' . mysqli_error($this->_link));
        }

        return $result;
    }

    public function fetch_one_value( $query ) {
       return mysqli_fetch_assoc( $this->run_query($query));
    }

}
/*
*/
