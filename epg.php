<?php
/**
 * Created by PhpStorm.
 * User: mussebekabil
 * Date: 11/09/17
 * Time: 16:10
 */
require_once __DIR__ . '/db_connection.php';
require_once __DIR__ . '/schedule.php';
require_once __DIR__ . '/program.php';
require_once __DIR__ . '/config.php';




class EPG {
    private $_existing_channels = array();
    protected $_connection = null;
    private $_config = null;

    public function __construct(){
        $this->setup();
    }

    /**
     * Initialize database connection and configuration file
     */
    public function setup() {
        $this->_connection = new DB_Connection();
        $this->_connection->open_connection();
        $this->_config = new Config();
    }

    /**
     * TODO: This should be moved to a separate channel model class
     * Returns auto generated channel id based on the api source id
     * @param $source_id
     * @return integer
     */
    public function get_channel_id( $source_id ) {
        $query = "select * from service_livetv_channel where `source_id` ='$source_id'";
        $channel = $this->_connection->fetch_one_value($query);

        return $channel['id'];
    }

    /**
     * Fetche existing channels
     */
    public function fetch_channels() {
        $query = 'select * from service_livetv_channel';
        $channels = $this->_connection->run_query($query);

        if( count($channels) > 0 ) {
            foreach( $channels as $channel ) {
                $channel_id = $channel['source_id'];
                if( !in_array($channel_id, $this->_existing_channels) ) {
                    $this->_existing_channels[] = $channel_id;
                }
            }
        }
    }

    /**
     * Build schedule object to send it to the database
     * @param $event
     * @param $channel_id
     * @param $ext_schedule_id
     * @return Schedule
     */
    public function build_schedule_obj( $event, $channel_id, $ext_schedule_id) {
        $duration = sprintf($event->duration);
        $end_time = $event->end_time ? sprintf($event->end_time) : null;
        $start_time = sprintf($event->start_time);

        $schedule = new Schedule($ext_schedule_id,
            $channel_id,
            $start_time,
            $end_time,
            $duration,
            null
        );

        return $schedule;
    }

    /**
     * Build program object to send it to the database
     * @param $short_event
     * @param $ext_program_id
     * @param $schedule_id
     * @return Program
     */
    public function build_program_obj( $short_event, $ext_program_id, $schedule_id) {
        $long_title = sprintf($short_event->name);
        $original_title = sprintf($short_event->text);
        $iso_2_lang = sprintf($short_event->language);

        $program = new Program( $ext_program_id,
                                null,
                                $long_title,
                                null,
                                $original_title,
                                null,
                                $iso_2_lang,
                                null,
                                $schedule_id
                            );
        return $program;

    }

    /**
     * Take xml data, map it to model objects and save it to the database
     */
    public function save_from_xml() {
        $data = simplexml_load_file($this->_config->getInputFile());
        $networks = $data->network;
        foreach( $networks as $network ) {
            foreach( $network->service as $service ) {
                $source_id = $service->attributes()->id;
                if( in_array($source_id, $this->_existing_channels) ) {
                    foreach( $service->event as $event ) {
                        $channel_id = $this->get_channel_id($source_id);
                        $event_attrs = $event->attributes();
                        $ext_schedule_id = $source_id .'-'. $event_attrs->id;
                        $schedule = $this->build_schedule_obj($event_attrs, $channel_id, $ext_schedule_id);
                        $schedule->save();
                        foreach( $event->language as $language ) {
                            $ext_program_id = $ext_schedule_id.'-'.$language->attributes()->code;
                            $schedule_id =  $schedule->get_schedule_id($ext_schedule_id);
                            $short_event = $language->short_event->attributes();
                            $program = $this->build_program_obj( $short_event, $ext_program_id, $schedule_id);
                            $program->save();
                        }
                    }
                }
            }
        }
    }

    /**
     * Take json data, map it to the model objects and save it to the database
     */
    public function save_from_json() {
        $data = json_decode(file_get_contents($this->_config->getInputFile()));
        $networks = $data->epg->network;
        foreach( $networks as $network ) {
            foreach( $network->service as $service ) {
                $source_id = $service->id;
                if( in_array($source_id, $this->_existing_channels) ) {
                    foreach( $service->event as $event ) {
                        $channel_id = $this->get_channel_id($source_id);
                        $ext_schedule_id = $source_id .'-'. $event->id;
                        $schedule = $this->build_schedule_obj($event, $channel_id, $ext_schedule_id);
                        $schedule->save();
                        $schedule_id =  $schedule->get_schedule_id($ext_schedule_id);
                        if( count($event->language) > 1 ) {
                            foreach( $event->language as $language ) {
                                $ext_program_id = $ext_schedule_id.'-'.$language->code;
                                $short_event = $language->short_event;
                                $program = $this->build_program_obj( $short_event, $ext_program_id, $schedule_id);
                                $program->save();
                            }
                        } else {
                            $ext_program_id = $ext_schedule_id.'-'.$event->language->code;
                            $short_event = $event->language->short_event;
                            $program = $this->build_program_obj( $short_event, $ext_program_id, $schedule_id);
                            $program->save();
                        }
                    }
                }
            }
        }
    }

    /**
     * TODO: Added support for web service input file format
     * Call to one of the save to database functions based on the selected input format
     */
    public function save_epg( ) {
        switch( $this->_config->getInputFileFormat() ) {
            case 'xml':
                $this->save_from_xml();
                break;
            case 'json':
                $this->save_from_json();
                break;
            case 'web_service':
                break;
            default:
                break;
        }
    }

}