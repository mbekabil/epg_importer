/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table service_livetv_channel
# ------------------------------------------------------------

CREATE TABLE `service_livetv_channel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(36) NOT NULL COMMENT 'UUID identifier',
  `source_id` int(11) NOT NULL COMMENT 'Metadata provider id',
  `short_name` varchar(30) NOT NULL COMMENT 'Short name for the channel',
  `full_name` varchar(128) NOT NULL COMMENT 'Full name for the channel',
  `time_zone` varchar(30) NOT NULL,
  `primary_language` varchar(2) DEFAULT NULL COMMENT 'Two character description for the channel',
  `weight` int(4) DEFAULT '0' COMMENT 'Listing weight for the channel',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `short_name` (`short_name`),
  UNIQUE KEY `uuid-unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


# Dump of table service_livetv_schedule
# ------------------------------------------------------------

CREATE TABLE `service_livetv_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ext_schedule_id` varchar(15) NOT NULL COMMENT 'Metadata provider schedule id',
  `channel_id` int(11) NOT NULL COMMENT 'Channel source/channel id',
  `start_time` datetime NOT NULL COMMENT 'Schedule start time',
  `end_time` datetime DEFAULT NULL COMMENT 'Schedule end time',
  `run_time` time NULL DEFAULT NULL COMMENT 'Schedule duration/run time',
  `is_live` tinyint(1) DEFAULT NULL COMMENT 'Is schedule a live broadcast',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_ext_schedule_id` (`ext_schedule_id`),
  UNIQUE KEY `index_channel_schedule` (`channel_id`,`start_time`,`end_time`),
  KEY `channel_id` (`channel_id`),
  CONSTRAINT `fk_service_livetv_schedule_channel_id` FOREIGN KEY (`channel_id`) REFERENCES `service_livetv_channel` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


# Dump of table service_livetv_program
# ------------------------------------------------------------

CREATE TABLE `service_livetv_program` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ext_program_id` varchar(15) NOT NULL COMMENT 'Metadata provider program id',
  `show_type` enum('movie','series','other') DEFAULT NULL COMMENT 'Program show type',
  `long_title` varchar(255) NOT NULL COMMENT 'Program long title',
  `grid_title` varchar(15) DEFAULT NULL COMMENT 'Program grid title',
  `original_title` varchar(255) DEFAULT NULL COMMENT 'Program original title',
  `duration` int(11) unsigned DEFAULT NULL COMMENT 'Program duration',
  `iso_3_lang` varchar(3) DEFAULT NULL COMMENT 'Program language',
  `eidr_id` varchar(50) DEFAULT NULL COMMENT 'Program Entertainment Identifier Registry',
  `schedule_id` int(11) NOT NULL COMMENT 'Schedule id',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `schedule_id` (`schedule_id`),
  KEY `indx_ext_program_id` (`ext_program_id`),
  CONSTRAINT `fk_service_livetv_schedule_id` FOREIGN KEY (`schedule_id`) REFERENCES `service_livetv_schedule` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  FULLTEXT KEY `indx_long_title` (`long_title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;







/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
