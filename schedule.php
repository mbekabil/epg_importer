<?php
class Schedule extends EPG {
    private $_ext_schedule_id;
    private $_channel_id;
    private $_start_time;
    private $_end_time;
    private $_run_time;
    private $_is_live;

    /**
     * @param mixed $ext_schedule_id
     */
    public function setExtScheduleId($ext_schedule_id)
    {
        $this->_ext_schedule_id = $ext_schedule_id;
    }

    /**
     * @param mixed $channel_id
     */
    public function setChannelId($channel_id)
    {
        $this->_channel_id = $channel_id;
    }

    /**
     * @param mixed $start_time
     */
    public function setStartTime($start_time)
    {
        $this->_start_time = $start_time;
    }

    /**
     * @param mixed $end_time
     */
    public function setEndTime($end_time)
    {
        $this->_end_time = $end_time;
    }

    /**
     * @param null $run_time
     */
    public function setRunTime($run_time)
    {
        $this->_run_time = $run_time;
    }

    /**
     * @param null $is_live
     */
    public function setIsLive($is_live)
    {
        $this->_is_live = $is_live;
    }

    public function __construct($ext_schedule_id,
                                $channel_id,
                                $start_time,
                                $end_time=NULL,
                                $run_time = NULL,
                                $is_live = NULL) {

        $this->_ext_schedule_id = $ext_schedule_id;
        $this->_channel_id = $channel_id;
        $this->_start_time = $start_time;
        $this->_end_time = $end_time;
        $this->_run_time = $run_time;
        $this->_is_live = $is_live;

        parent::setup();
    }

    /**
     * Return schedule auto generated id base on the api external schedule id
     * @param $ext_schedule_id
     * @return integer
     */
    public function get_schedule_id( $ext_schedule_id ) {
        $query = "select * from service_livetv_schedule where `ext_schedule_id` ='$ext_schedule_id'";
        $schedule = $this->_connection->fetch_one_value($query);

        return $schedule['id'];
    }

    /**
     * Save schedule model to service_livetv_schedule table
     */
    public function save() {
        if( !$this->_ext_schedule_id ) {
            die('Error: Schedule is mandatory');
        }
        if( !$this->_channel_id ) {
            die('Error: Channel is mandatory');
        }
        if( !$this->_start_time ) {
            die('Error: Start time is mandatory');
        }

        $query = sprintf('INSERT INTO `service_livetv_schedule`'.
                         '(`ext_schedule_id`, `channel_id`, `start_time` %1$s %2$s %3$s)'.
                         'VALUES ("%4$s", %5$s, "%6$s" %7$s %8$s %9$s);',
                         $this->_end_time ? ', `end_time`' : '',
                         $this->_run_time ? ', `run_time`' : '',
                         $this->_is_live ? ', `is_live`' : '',
                         $this->_ext_schedule_id,
                         $this->_channel_id,
                         $this->_start_time,
                         $this->_end_time ? ', "' . $this->_end_time . '"': '',
                         $this->_run_time ? ', "' . $this->_run_time . '"': '',
                         $this->_is_live ? ', "' . $this->_is_live . '"': ''
                        );
        $this->_connection->run_query($query);
    }
}