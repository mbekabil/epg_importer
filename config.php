<?php
class Config {
    private $_db_host = 'localhost';
    private $_db_database = 'EPG';
    private $_db_user_name = 'root';
    private $_db_password = 'root';
    private $_input_file_format = 'json';
    private $_input_file = 'kuivuri.json';

    /**
     * @return string
     */
    public function getDbHost()
    {
        return $this->_db_host;
    }

    /**
     * @param string $db_host
     */
    public function setDbHost($db_host)
    {
        $this->_db_host = $db_host;
    }

    /**
     * @return string
     */
    public function getDbDatabase()
    {
        return $this->_db_database;
    }

    /**
     * @param string $db_database
     */
    public function setDbDatabase($db_database)
    {
        $this->_db_database = $db_database;
    }

    /**
     * @return string
     */
    public function getDbUserName()
    {
        return $this->_db_user_name;
    }

    /**
     * @param string $db_user_name
     */
    public function setDbUserName($db_user_name)
    {
        $this->_db_user_name = $db_user_name;
    }

    /**
     * @return string
     */
    public function getDbPassword()
    {
        return $this->_db_password;
    }

    /**
     * @param string $db_password
     */
    public function setDbPassword($db_password)
    {
        $this->_db_password = $db_password;
    }

    /**
     * @return string
     */
    public function getInputFileFormat()
    {
        return $this->_input_file_format;
    }

    /**
     * @param string $input_file_format
     */
    public function setInputFileFormat($input_file_format)
    {
        $this->_input_file_format = $input_file_format;
    }

    /**
     * @return string
     */
    public function getInputFile()
    {
        return $this->_input_file;
    }

    /**
     * @param string $input_file
     */
    public function setInputFile($input_file)
    {
        $this->_input_file = $input_file;
    }

}