<?php
class Program extends EPG {
    private $_ext_program_id;
    private $_show_type;
    private $_long_title;
    private $_grid_title;
    private $_original_title;
    private $_duration;
    private $_iso_3_lang;
    private $_eidr_id;
    private $_schedule_id;


    /**
     * @param mixed $ext_program_id
     */
    public function setExtProgramId($ext_program_id)
    {
        $this->_ext_program_id = $ext_program_id;
    }

    /**
     * @param mixed $show_type
     */
    public function setShowType($show_type)
    {
        $this->_show_type = $show_type;
    }

    /**
     * @param mixed $long_title
     */
    public function setLongTitle($long_title)
    {
        $this->_long_title = $long_title;
    }

    /**
     * @param null $grid_title
     */
    public function setGridTitle($grid_title)
    {
        $this->_grid_title = $grid_title;
    }

    /**
     * @param null $original_title
     */
    public function setOriginalTitle($original_title)
    {
        $this->_original_title = $original_title;
    }

    /**
     * @param null $duration
     */
    public function setDuration($duration)
    {
        $this->_duration = $duration;
    }

    /**
     * @param null $iso_2_lang
     */
    public function setIso3Lang($iso_3_lang)
    {
        $this->_iso_3_lang = $iso_3_lang;
    }

    /**
     * @param null $eidr_id
     */
    public function setEidrId($eidr_id)
    {
        $this->_eidr_id = $eidr_id;
    }

    /**
     * @param mixed $schedule_id
     */
    public function setScheduleId($schedule_id)
    {
        $this->_schedule_id = $schedule_id;
    }

    public function __construct( $ext_program_id,
                                 $show_type,
                                 $long_title,
                                 $grid_title = null,
                                 $original_title = null,
                                 $duration = null,
                                 $iso_2_lang = null,
                                 $eidr_id = null,
                                 $schedule_id) {

        $this->_ext_program_id = $ext_program_id;
        $this->_show_type = $show_type;
        $this->_long_title = $long_title;
        $this->_grid_title = $grid_title;
        $this->_original_title = $original_title;
        $this->_duration = $duration;
        $this->_iso_3_lang = $iso_2_lang;
        $this->_eidr_id = $eidr_id;
        $this->_schedule_id = $schedule_id;

        parent::setup();

    }

    /**
     * Save program model to service_livetv_program table
     */
    public function save() {
        if( !$this->_ext_program_id ) {
            die('Error: Program is mandatory');
        }

        if( !$this->_long_title ) {
            die('Error: Title is mandatory');
        }
        if( !$this->_schedule_id ) {
            die('Error: Schedule is mandatory');
        }

        $query = sprintf('INSERT INTO `service_livetv_program`'.
                      '(`ext_program_id`,`long_title`, `schedule_id`, `iso_3_lang` %1$s  %2$s %3$s %4$s %5$s)'.
                      ' VALUES ( "%6$s", "%7$s", %8$s, "%9$s" %10$s %11$s %12$s %13$s %14$s );',
                      $this->_show_type ? ', `show_type`' : '',
                      $this->_original_title ? ', `original_title`' : '',
                      $this->_grid_title ? ', `grid_title`' : '',
                      $this->_eidr_id ? ', `eidr_id`' : '',
                      $this->_duration ? ', `duration`' : '',
                      $this->_ext_program_id,
                      mysqli_real_escape_string($this->_long_title),
                      $this->_schedule_id,
                      $this->_iso_3_lang,
                      $this->_show_type ? ', "' . $this->_show_type . '"': '',
                      $this->_grid_title ? ',"'. mysqli_real_escape_string($this->_grid_title) .'"' : '',
                      $this->_original_title ? ', "'. mysqli_real_escape_string($this->_original_title) .'"' : '',
                      $this->_eidr_id ? ', "'. $this->_eidr_id . '"': '',
                      $this->_duration ? ', "' . $this->_duration . '"' : '' );
        $this->_connection->run_query($query);
    }

}